  $(function() {
      $('[data-toggle="tooltip"]').tooltip();
  });
  var statusData = [];
  var layoutData = [];
  var nameArr = [];

  function fetchData() {
      $.getJSON('http://dev-olap.nextscm.com:8080/increff/ap', function(data) {
          layoutData = data;
          for (let i = 0; i < data.length; i++) {
              for (let j = 0; j < data[i].modules.length; j++) {
                  for (let k = 0; k < data[i].modules[j].steps.length; k++) {
                      nameArr.push(data[i].modules[j].steps[k].name);
                  }
              }
          }
          getStatus();
      });
  }
  function getStatus(){
    $.ajax({
              url: "http://dev-olap.nextscm.com:8080/increff/status",
              type: "POST",
              contentType: "application/json",
              data: JSON.stringify(nameArr),
              success: function(data) {
                  statusData = data;
                  createLayout(layoutData);
              },
              error: function(data) {
                  showToast("Something went wrong! ");
              },
              xhrFields: {
                  withCredentials: true
              },
              crossDomain: true
          });
  }
//   function createLayout(data) {
//       for (let j = 0; j < data.length; j++) {

//           for (let k = 0; k < data[j].modules.length; k++) {
//               var panel_sub = $('<div></div>').addClass('panel-sub');
//               $(wrapper).append(panel_sub);
//               var subStep = $('<div></div>');
//               $(subStep).append($('<p></p>').text(data[j].modules[k].name).addClass('p-width')).addClass('sub-step');

//               $(panel_sub).append(subStep);
//               var step = $('<div></div>').addClass('step');
//               $(panel_sub).append(step);
//               for (let i = 0; i < data[j].modules[k].steps.length; i++) {
//                   var node = createBubble(i, data[j].modules[k].steps[i], data[j].modules[k].steps.length);
//                   $(step).append(node);

//               }
//           }
//       }
//       $('[data-toggle="tooltip"]').tooltip();
//       var layoutData = data;

//   }
    function createLayout(data){
        for(let i=0;i<data.length;i++){
            
        }
    }
  function createBubble(i, step, length) {
      var node = $("<div></div>").addClass("bubble");
      var node_circle = $("<div></div>").addClass("bubble-circle");
      $(node_circle).attr("data-toggle", "tooltip").attr("data-placement", "bottom").attr("data-html", "true");
      createTooltip(node_circle, step);
      $(node_circle).click(function() {
          $.ajax({
              url: "http://dev-olap.nextscm.com:8080/increff/algo?algo=" + step.name,
              type: "POST",
              success: function(data) {
                  showToast("Algorithm ran successfully");
                  getStatus();
                  $(".tooltip").removeClass("show");
              },
              error: function(data) {
                  showToast("Something went wrong! ");
              },
              xhrFields: {
                  withCredentials: true
              },
              crossDomain: true
          });
      });
      var node_text = $("<p></p>").text(step.number).addClass("bubble-text");
      var node_wrap = $("<div></div>").addClass("bubble-wrap");
      $(node_wrap).append(node_circle);
      if (i != length - 1) {
          var branch = $("<div></div>").addClass("branch-hr");
          $(node_wrap).append(branch);
      }
      $(node).append(node_wrap, node_text);
      return node;
  }
  function showToast(str) {
      var x = $("#snackbar").addClass("show").text(str);
      setTimeout(function() {
          $("#snackbar").removeClass("show");
      }, 3000);
  }

  function createTooltip(node_circle, step) {
      for (let i = 0; i < statusData.length; i++) {
          if (statusData[i].name.toUpperCase() == step.name.toUpperCase()) {
              if (statusData[i].lastRan) {
                  var date = new Date(statusData[i].lastRan);
                  date = date.toString();
              } else {
                  date = "Not Available";
              }

              let title = "Name:" + statusData[i].name + "<br/>" + "Status: " + statusData[i].status + "<br/>Last Ran: " + date + "<br/>Time Taken: " + (statusData[i].timeTaken / 1000) + " seconds";
              $(node_circle).attr("title", title).addClass(statusData[i].status);
          }
      }
  }

  

  fetchData();