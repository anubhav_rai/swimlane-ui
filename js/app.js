'use strict';

var myApp= angular.module('swimApp',['ngRoute']);

myApp.config(function($routeProvider){
	$routeProvider
		.when('/',{
			templateUrl: 'views/modules.html',
			controller: 'MainCtrl'
		})
        .when('/users',{
			templateUrl: 'views/users.html',
			controller: 'MainCtrl'
		})
        .when('/settings',{
			templateUrl: 'views/settings.html',
			controller: 'MainCtrl'
		})
		.otherwise({
			redirectTo: '/'
		});
}).run(['$rootScope',  function() {}]);

myApp.controller('MainCtrl',function($scope,$http){
    $scope.layoutData=[];
    $scope.nameArr=[];
    $scope.statusData=[];
    $scope.heading="Hello World";
    $scope.head="AP";
    $scope.subItem="Run";
    $scope.changeHead= function(obj){
        $scope.head=obj.name;
        
    }
    $scope.subAP= function(str){
        $scope.subItem= str;
    }
    $http.get('http://dev-olap.nextscm.com:8080/increff/ap').then(function(response){
        $scope.layoutData = response.data;
        console.log("layoutData is "+JSON.stringify($scope.layoutData));
        for (let i = 0; i < response.data.length; i++) {
              for (let j = 0; j < response.data[i].modules.length; j++) {
                  for (let k = 0; k < response.data[i].modules[j].steps.length; k++) {
                      $scope.nameArr.push(response.data[i].modules[j].steps[k].name);
                  }
              }
          }
          $scope.nameArr=JSON.stringify($scope.nameArr);
          $scope.getStatus();
    });
    $scope.getStatus= function(){
        $http({
            method: "POST",
            url: "http://dev-olap.nextscm.com:8080/increff/status",
            headers:{'contentType': "application/json"},
            data: $scope.nameArr 
        }).then(function(response){
            $scope.statusData= response.data;
            $scope.createLayout($scope.layoutData);
        },function(response){
            alert("Something went wrong!!");
        });
    }
    $scope.createLayout=function(){

    }
    
});
